﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMApplication.Entities
{
    public class Employee
    {
        public string Emp_id { get; set; }
        public string Emp_name { get; set; }
        public DateTime Birth_date { get; set; }
        public DateTime Join_date { get; set; }
        public string Gender { get; set; }
        public string Designation { get; set; }
    }
}
