﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using HRMApplication.DAL;
using System.Data.SqlClient;
using CrystalDecisions.CrystalReports.Engine;

namespace HRMApplication
{
    public partial class AllOrderInvoce : Form
    {
        public AllOrderInvoce()
        {
            InitializeComponent();
        }

        ReportDocument rd = new ReportDocument();
        private void AllOrderInvoce_Load(object sender, EventArgs e)
        {
            DisplayInvoce();
        }
        void DisplayInvoce()
        {
            rd.Load(@"E:\Dot Net\Practice\WindowsForm\HRMApplication\HRMApplication\rpt_all_salesOrder.rpt");
            SqlCommand cmd = new SqlCommand("wsp_rpt_all_salesOrder", DBConfiguration.Conn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds, "wsp_rpt_all_salesOrder");
            rd.SetDataSource(ds);
            crystalReportViewer1.ReportSource = rd;

        }
    }
}
