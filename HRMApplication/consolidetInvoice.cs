﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using HRMApplication.DAL;
using CrystalDecisions.CrystalReports.Engine;

namespace HRMApplication
{
    public partial class consolidetInvoice : Form
    {
        public consolidetInvoice()
        {
            InitializeComponent();
        }

        ReportDocument rd = new ReportDocument();
        private void button1_Click(object sender, EventArgs e)
        {
            
            if (txtinvoiceNo.Text !=null )
            {
                rd.Load(@"E:\Dot Net\Practice\WindowsForm\HRMApplication\HRMApplication\rpt_consolidetInvoice.rpt");
                SqlCommand cmd = new SqlCommand("wsp_rpt_salesOrder", DBConfiguration.Conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandTimeout = 10000;
                cmd.Parameters.AddWithValue("@invoiceId", Convert.ToInt32(txtinvoiceNo.Text));
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds, "wsp_rpt_salesOrder");
                rd.SetDataSource(ds);
                crystalReportViewer1.ReportSource = rd;
                crystalReportViewer1.Refresh();

                


            }
            else
            {
                MessageBox.Show("Enter Invoice Id");
            }

        }
    }
}
