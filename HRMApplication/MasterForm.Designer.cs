﻿namespace HRMApplication
{
    partial class MasterForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.homeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.registrationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.employeeInfoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.reportsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.employeeListToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.consolideteEmployeeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.salseProductToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.allOrderReportsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.consolidateInvoiceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.menuStrip1.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.homeToolStripMenuItem,
            this.registrationToolStripMenuItem,
            this.reportsToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(956, 25);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // homeToolStripMenuItem
            // 
            this.homeToolStripMenuItem.Name = "homeToolStripMenuItem";
            this.homeToolStripMenuItem.Size = new System.Drawing.Size(55, 21);
            this.homeToolStripMenuItem.Text = "Home";
            // 
            // registrationToolStripMenuItem
            // 
            this.registrationToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.employeeInfoToolStripMenuItem});
            this.registrationToolStripMenuItem.Name = "registrationToolStripMenuItem";
            this.registrationToolStripMenuItem.Size = new System.Drawing.Size(90, 21);
            this.registrationToolStripMenuItem.Text = "Registration";
            // 
            // employeeInfoToolStripMenuItem
            // 
            this.employeeInfoToolStripMenuItem.Name = "employeeInfoToolStripMenuItem";
            this.employeeInfoToolStripMenuItem.Size = new System.Drawing.Size(159, 22);
            this.employeeInfoToolStripMenuItem.Text = "Employee Info";
            this.employeeInfoToolStripMenuItem.Click += new System.EventHandler(this.employeeInfoToolStripMenuItem_Click);
            // 
            // reportsToolStripMenuItem
            // 
            this.reportsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.employeeListToolStripMenuItem,
            this.consolideteEmployeeToolStripMenuItem,
            this.salseProductToolStripMenuItem,
            this.allOrderReportsToolStripMenuItem,
            this.consolidateInvoiceToolStripMenuItem});
            this.reportsToolStripMenuItem.Name = "reportsToolStripMenuItem";
            this.reportsToolStripMenuItem.Size = new System.Drawing.Size(66, 21);
            this.reportsToolStripMenuItem.Text = "Reports";
            // 
            // employeeListToolStripMenuItem
            // 
            this.employeeListToolStripMenuItem.Name = "employeeListToolStripMenuItem";
            this.employeeListToolStripMenuItem.Size = new System.Drawing.Size(206, 22);
            this.employeeListToolStripMenuItem.Text = "Employee List";
            this.employeeListToolStripMenuItem.Click += new System.EventHandler(this.employeeListToolStripMenuItem_Click);
            // 
            // consolideteEmployeeToolStripMenuItem
            // 
            this.consolideteEmployeeToolStripMenuItem.Name = "consolideteEmployeeToolStripMenuItem";
            this.consolideteEmployeeToolStripMenuItem.Size = new System.Drawing.Size(206, 22);
            this.consolideteEmployeeToolStripMenuItem.Text = "Consolidete Employee";
            // 
            // salseProductToolStripMenuItem
            // 
            this.salseProductToolStripMenuItem.Name = "salseProductToolStripMenuItem";
            this.salseProductToolStripMenuItem.Size = new System.Drawing.Size(206, 22);
            this.salseProductToolStripMenuItem.Text = "Salse Product";
            this.salseProductToolStripMenuItem.Click += new System.EventHandler(this.salseProductToolStripMenuItem_Click);
            // 
            // allOrderReportsToolStripMenuItem
            // 
            this.allOrderReportsToolStripMenuItem.Name = "allOrderReportsToolStripMenuItem";
            this.allOrderReportsToolStripMenuItem.Size = new System.Drawing.Size(206, 22);
            this.allOrderReportsToolStripMenuItem.Text = "All Order Reports";
            this.allOrderReportsToolStripMenuItem.Click += new System.EventHandler(this.allOrderReportsToolStripMenuItem_Click);
            // 
            // consolidateInvoiceToolStripMenuItem
            // 
            this.consolidateInvoiceToolStripMenuItem.Name = "consolidateInvoiceToolStripMenuItem";
            this.consolidateInvoiceToolStripMenuItem.Size = new System.Drawing.Size(206, 22);
            this.consolidateInvoiceToolStripMenuItem.Text = "Consolidate Invoice";
            this.consolidateInvoiceToolStripMenuItem.Click += new System.EventHandler(this.consolidateInvoiceToolStripMenuItem_Click);
            // 
            // MasterForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(956, 448);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "MasterForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "MasterForm";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem homeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem registrationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem employeeInfoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem reportsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem employeeListToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem consolideteEmployeeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem salseProductToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem allOrderReportsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem consolidateInvoiceToolStripMenuItem;
    }
}