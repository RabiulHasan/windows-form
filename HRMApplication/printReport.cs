﻿using CrystalDecisions.CrystalReports.Engine;
using HRMApplication.DAL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HRMApplication
{
    public partial class printReport : Form
    {
        public printReport()
        {
            InitializeComponent();
        }

        private void printReport_Load(object sender, EventArgs e)
        {
            ReportDocument rprt = new ReportDocument();
            rprt.Load(@"E:\\Dot Net\Practice\WindowsForm\HRMApplication\HRMApplication\rpt_employeeInfo.rpt");

            SqlCommand cmd = new SqlCommand("wsp_rpt_employeeinfo", DBConfiguration.Conn);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            sda.Fill(ds, "wsp_rpt_employeeinfo");
            rprt.SetDataSource(ds);
            crystalReportViewer1.ReportSource = rprt;
        }

    }
}
