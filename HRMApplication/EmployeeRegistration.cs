﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using HRMApplication.DAL;
using HRMApplication.Entities;
using CrystalDecisions.CrystalReports.Engine;

namespace HRMApplication
{
    public partial class EmployeeRegistration : Form
    {
        Transaction objTran = new Transaction();
        public EmployeeRegistration()
        {
            InitializeComponent();
            LoadDesignation();
            LoadDataGridView();
        }

         void LoadDesignation()
        {
            DataTable dt = objTran.FetchData("Select id,category_name from category where status='Y' ");
            cmbDesignation.DataSource = dt;
            cmbDesignation.ValueMember = "id";
            cmbDesignation.DisplayMember = "category_name";
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            AddEmployee();
            LoadDataGridView();
        }

        private void LoadDataGridView()
        {
            DataTable dt = objTran.FetchData("Select emp_id,emp_name,gender,birth_date,join_date,designation from employee_registration");
            dgvEmployee.DataSource=dt;
        }

        void AddEmployee()
        {
            try
            {

                if (txtEmpId.Text != null)
                {
                    Employee emp = new Employee();
                    emp.Emp_id = txtEmpId.Text;
                    emp.Emp_name = txtEmpName.Text;
                    emp.Gender = cmbGender.SelectedItem.ToString();
                    emp.Birth_date = Convert.ToDateTime(dtpBirthDate.Text);
                    emp.Join_date = Convert.ToDateTime(dtpJoindate.Text);
                    emp.Designation = cmbDesignation.SelectedValue.ToString();

                    int row = objTran.AddEditEmployee(emp);
                    if (row > 0)
                    {
                        MessageBox.Show("Operation Successfully");
                    }
                    else
                    {
                        MessageBox.Show("Not Saved");
                    }

                }
                else
                {

                    MessageBox.Show("Field Is Empty");

                }

            }
            //catch (Exception ex)
            //{

            //    throw ex;
            //}
            finally { }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            if (txtSearch.Text != null)
            {
                string empid = txtSearch.Text;
                DataTable dt = objTran.FetchData("Select emp_id,emp_name,gender,birth_date,join_date,designation from employee_registration where emp_id='"+ empid+"'");
                dgvEmployee.DataSource = dt;

            }
            else
            {
                MessageBox.Show("Faild");
            }
        }

        private void dgvEmployee_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            txtEmpId.Text = dgvEmployee.SelectedRows[0].Cells[0].Value.ToString();
            txtEmpName.Text = dgvEmployee.SelectedRows[0].Cells[1].Value.ToString();
            cmbGender.SelectedItem = dgvEmployee.SelectedRows[0].Cells[2].Value.ToString();
            dtpBirthDate.Text = dgvEmployee.SelectedRows[0].Cells[3].Value.ToString();
            dtpJoindate.Text = dgvEmployee.SelectedRows[0].Cells[4].Value.ToString();
            cmbDesignation.SelectedItem = dgvEmployee.SelectedRows[0].Cells[5].Value.ToString();
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            AddEmployee();
        }

        private void btnFetch_Click(object sender, EventArgs e)
        {
            LoadDataGridView();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (txtEmpId.Text != null || txtEmpId.Text != "")
            {

                string sql =objTran.DeleteInfo("Delete from employee_registration where emp_id='" + txtEmpId.Text + "'");
         
              
                MessageBox.Show("Delete Successfully");
            }
            else {
                MessageBox.Show("Faild");
            }
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            Clear();
        }

        void Clear()
        {
            txtEmpId.Text = "";
            txtEmpName.Text = "";
            
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {

        }
    }
}
