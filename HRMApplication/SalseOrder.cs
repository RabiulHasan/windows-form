﻿using HRMApplication.DAL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using CrystalDecisions.CrystalReports.Engine;
using System.Windows.Forms;

namespace HRMApplication
{
    public partial class SalseOrder : Form
    {
        public SalseOrder()
        {
            InitializeComponent();
        }

        private void SalseOrder_Load(object sender, EventArgs e)
        {

        }

        private void btnAdd_Click(object sender, EventArgs e)
        {

            int row = 0;
            dgvProduct.Rows.Add();
            row = dgvProduct.Rows.Count - 2;
            dgvProduct["invoiceNo",row].Value = Convert.ToInt32(txtInvoice.Text);            
            dgvProduct["productName", row].Value = txtProduct.Text.ToString();
            dgvProduct["quantity",row].Value = Convert.ToInt32(txtquantity.Text);
            dgvProduct["price", row].Value = Convert.ToInt32(txtprice.Text);


                 

        }

        private void btnInvoice_Click(object sender, EventArgs e)
        {
            for (int i=0; i<=dgvProduct.Rows.Count-2;i++)
            {
                Transaction obj = new Transaction();

                int result = obj.InsertData(Convert.ToInt32(dgvProduct.Rows[i].Cells[0].Value),dgvProduct.Rows[i].Cells[1].Value.ToString(), Convert.ToInt32(dgvProduct.Rows[i].Cells[2].Value),Convert.ToInt32(dgvProduct.Rows[i].Cells[3].Value));
                if (result > 0)
                {
                    MessageBox.Show("Successfully");
                }
                else
                {
                    MessageBox.Show("Failed");
                }


            }
        }

        private void btnPrintInvoice_Click(object sender, EventArgs e)
        {
            ReportDocument rd = new ReportDocument();
            if (txtInvoice.Text !=null || txtInvoice.Text!="")
            {
                rd.Load(@"E:\Dot Net\Practice\WindowsForm\HRMApplication\HRMApplication\rpt_consolidetInvoice.rpt");
                SqlCommand cmd = new SqlCommand("wsp_rpt_salesOrder", DBConfiguration.Conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@invoiceId", Convert.ToInt32(txtInvoice.Text));
                
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds, "wsp_rpt_salesOrder");
                rd.SetDataSource(ds);
                rd.SetParameterValue("cusInvoice",txtInvoice.Text);
                crystalReportViewer1.ReportSource = rd;

            }
            else {
                MessageBox.Show("Enter Invoice No");
            }
        }
    }
}
