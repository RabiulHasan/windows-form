﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using HRMApplication.DAL;
using HRMApplication.Entities;
namespace HRMApplication.DAL
{
    
    public class Transaction
    {
        SqlCommand cmd;
        DataTable dt;
        SqlDataAdapter da;
        public Transaction()
        { 
        }


 
        public DataTable ExecuteDataTable(string command)
        {
            try
            {
                DBConfiguration.OpenConnection();                
                cmd = new SqlCommand(command,DBConfiguration.Conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandTimeout = 10000;
                da = new SqlDataAdapter(cmd);
                dt = new DataTable();
                da.Fill(dt);
                return dt;

            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                DBConfiguration.CloseConnection();
            }
        }

        public DataTable FetchData(string query)
        {
            try
            {
                DBConfiguration.OpenConnection();
                da = new SqlDataAdapter(query, DBConfiguration.Conn);
                dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally 
            {
                DBConfiguration.CloseConnection();
            }
        
        }

        internal int InsertData(int invoiceNo, string productName, int quantity, int price)
        {
            DBConfiguration.OpenConnection();
            var query = "Insert into salesOrder(invoiceNo,productName,quantity,price) values('" + invoiceNo+"','" + productName + "','"+ quantity + "','"+price+"')";
            SqlCommand cmd = new SqlCommand(query, DBConfiguration.Conn);
            cmd.CommandType = CommandType.Text;
            cmd.CommandTimeout = 10000;
           int row= cmd.ExecuteNonQuery();
          
            DBConfiguration.CloseConnection();
            return row;
        }

        internal int AddEditEmployee(Employee emp)
        {
            using(cmd=new SqlCommand("wsp_addEdit_employee",DBConfiguration.Conn))
            {

                try
                {
                    DBConfiguration.OpenConnection();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandTimeout = 10000;
                    cmd.Parameters.AddWithValue("@empid", emp.Emp_id);
                    cmd.Parameters.AddWithValue("@empname", emp.Emp_name);
                    cmd.Parameters.AddWithValue("@gender", emp.Gender);
                    cmd.Parameters.AddWithValue("@birthDate", emp.Birth_date);
                    cmd.Parameters.AddWithValue("@joindate", emp.Join_date);
                    cmd.Parameters.AddWithValue("@designation", emp.Designation);
                    int row = cmd.ExecuteNonQuery();
                    return row;
                }
                catch (Exception ex)
                {

                    throw ex;
                }
                finally 
                {
                    DBConfiguration.CloseConnection();

                }
            }
           
           
        }

        internal string DeleteInfo(string query)
        {
            try
            {
                DBConfiguration.OpenConnection();
                SqlCommand cmd = new SqlCommand(query, DBConfiguration.Conn);
                cmd.CommandType = CommandType.Text;
                cmd.CommandTimeout = 10000;
                cmd.ExecuteNonQuery();
                return query;
            }
            finally
            {
                DBConfiguration.CloseConnection();
            }
            
           
        }
    }
}
