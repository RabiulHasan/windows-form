﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Data.SqlClient;
using HRMApplication.DAL;

namespace HRMApplication.DAL
{
   public class DBConfiguration
    {
      private static readonly SqlConnection conn;
       static DBConfiguration()
       {
           string sqlcon = ConfigurationManager.ConnectionStrings["DBConn"].ConnectionString;
           conn = new SqlConnection(sqlcon);
       
       }

       public static void OpenConnection()
       {
           conn.Open();
       }
       public static void CloseConnection()
       {
           conn.Close();
       }
       public static SqlConnection Conn
       {
           get
           {
               return conn;
           }
       }


    }
}
